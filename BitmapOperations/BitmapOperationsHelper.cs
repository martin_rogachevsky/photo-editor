﻿using System;
using System.Drawing;
using System.Threading;

namespace BitmapOperations
{
    public static class BitmapOperationsHelper
    {
        private static Color EditColorSaturation(this Color color, double s)
        {
            double l = color.GetBrightness(), h = color.GetHue() / 360.0;
            s *= color.GetSaturation();
            s = s > 1 ? 1 : s;
            double r, g, b;

            if (Math.Abs(l) < 0.001)
            {
                r = g = b = 0;
            }
            else
            {
                if (Math.Abs(s) < 0.001)
                {
                    r = g = b = l;
                }
                else
                {
                    var temp2 = l <= 0.5 
                        ? l * (1.0 + s) 
                        : l + s - l * s;
                    var temp1 = 2.0 * l - temp2;

                    double[] t3 = { h + 1.0 / 3.0, h, h - 1.0 / 3.0 };
                    double[] clr = { 0, 0, 0 };
                    for (int i = 0; i < 3; i++)
                    {
                        if (t3[i] < 0)
                            t3[i] += 1.0;
                        if (t3[i] > 1)
                            t3[i] -= 1.0;

                        if (6.0 * t3[i] < 1.0)
                            clr[i] = temp1 + (temp2 - temp1) * t3[i] * 6.0;
                        else if (2.0 * t3[i] < 1.0)
                            clr[i] = temp2;
                        else if (3.0 * t3[i] < 2.0)
                            clr[i] = (temp1 + (temp2 - temp1) * ((2.0 / 3.0) - t3[i]) * 6.0);
                        else
                            clr[i] = temp1;
                    }
                    r = clr[0];
                    g = clr[1];
                    b = clr[2];
                }
            }

            return Color.FromArgb(color.A, (int)(255 * r), (int)(255 * g), (int)(255 * b));
        }

        public static void Contrast(this Bitmap workerBitmap, int value)
        {
            var contrast = (100.0 + value) / 100.0;
            contrast *= contrast;

            byte[] palette = new byte[256];
            double rgb;

            for (int i = 0; i < 256; i++)
            {
                rgb = (i - 128) * contrast + 128;
                palette[i] = (byte) (rgb > 255
                    ? 255
                    : (rgb < 0 ? 0 : rgb));
            }

            using (var lockedSource = new LockBitmap(workerBitmap))
            {
                lock (workerBitmap)
                {
                    lockedSource.LockBits();

                    Color p;
                    for (int y = 0; y < lockedSource.Height; y++)
                    {
                        for (int x = 0; x < lockedSource.Width; x++)
                        {
                            p = lockedSource.GetPixel(x, y);
                            lockedSource.SetPixel(x, y, Color.FromArgb(p.A, palette[p.R], palette[p.G], palette[p.B]));
                        }
                    }
                    lockedSource.UnlockBits();
                }
            }
        }

        public static void Saturation(this Bitmap workerBitmap, int value)
        {
            void ComputeArea(int x, int y, int width, int height, LockBitmap lockedSource, double sat)
            {
                Color p;
                for (int i = y; i < height; i++)
                {
                    for (int j = x ; j < width; j++)
                    {
                        p = lockedSource.GetPixel(j, i);
                        lockedSource.SetPixel(j, i, p.EditColorSaturation(sat));
                    }
                }
            }

            var s = (100.0 + value) / 100.0;

            using (var lockedSource = new LockBitmap(workerBitmap))
            {
                lock (workerBitmap)
                {
                    lockedSource.LockBits();

                    var part1Worker = new Thread(() =>
                        ComputeArea(0, 0, lockedSource.Width, lockedSource.Height / 4, lockedSource, s));
                    var part2Worker = new Thread(() => ComputeArea(0, lockedSource.Height / 4, lockedSource.Width,
                        lockedSource.Height / 2, lockedSource, s));
                    var part3Worker = new Thread(() => ComputeArea(0, lockedSource.Height / 2, lockedSource.Width,
                        lockedSource.Height * 3 / 4, lockedSource, s));
                    part1Worker.Start();
                    part2Worker.Start();
                    part3Worker.Start();
                    ComputeArea(0, lockedSource.Height * 3 / 4, lockedSource.Width, lockedSource.Height, lockedSource,
                        s);

                    part1Worker.Join();
                    part2Worker.Join();
                    part3Worker.Join();
                    lockedSource.UnlockBits();
                }
            }
        }

        public static void Brightness(this Bitmap workerBitmap, int value)
        {
            int brightness = (int) (value / 100.0 * 255);

            byte[] palette = new byte[256];
            double rgb;

            for (int i = 0; i < 256; i++)
            {
                rgb = i + brightness;
                palette[i] = (byte) (rgb > 255
                    ? 255
                    : (rgb < 0 ? 0 : rgb));
            }

            using (var lockedSource = new LockBitmap(workerBitmap))
            {
                lock (workerBitmap)
                {
                    lockedSource.LockBits();

                    Color p;
                    for (int y = 0; y < lockedSource.Height; y++)
                    {
                        for (int x = 0; x < lockedSource.Width; x++)
                        {
                            p = lockedSource.GetPixel(x, y);
                            lockedSource.SetPixel(x, y, Color.FromArgb(p.A, palette[p.R], palette[p.G], palette[p.B]));
                        }
                    }

                    lockedSource.UnlockBits();
                }
            }
        }

        public static Bitmap Rotate(this Bitmap bitmap, double angle)
        {
            Graphics g = Graphics.FromImage(bitmap);
            g.TranslateTransform(bitmap.Width / 2f, bitmap.Height / 2f);
            g.RotateTransform((float)angle);
            g.TranslateTransform(-bitmap.Width / 2f, -bitmap.Height / 2f);
            g.DrawImage(bitmap, new Point(0, 0));
            g.Dispose();

            double scale = Math.Abs(1 / (float) (Math.Abs(Math.Sin(angle * Math.PI / 180.0)) * bitmap.Width / bitmap.Height +
                                                 Math.Abs(Math.Cos(angle * Math.PI / 180.0))));
            int newWidth = (int)(scale * bitmap.Width);
            int newHeight = (int)(scale * bitmap.Height);
            return bitmap.Clone(new Rectangle((int) (bitmap.Width / 2f - newWidth / 2f),
                (int) (bitmap.Height / 2f - newHeight / 2f),
                newWidth, newHeight), bitmap.PixelFormat);
        }

    }
}