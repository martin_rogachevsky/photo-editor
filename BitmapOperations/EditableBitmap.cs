﻿using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;

namespace BitmapOperations
{
    public class EditableBitmap
    {
        private readonly Bitmap _source;
        private Bitmap _result;
        private double _rotateAngle;

        public double RotateAngle
        {
            get => _rotateAngle;
            set => _rotateAngle = value > 360 ? 360 : value < 0 ? 0 : value;
        }

        public EditableBitmap(Bitmap source)
        {
            _source = source;
            _result = (Bitmap)_source.Clone();
        }

        public Bitmap GetBitmap()
        {
            return TranformBitmap();
        }

        public void EditBitmap(int contrast, int brightness, int color)
        {
            Bitmap workerBitmap;
            lock (_source)
            {
                workerBitmap = new Bitmap((Bitmap)_source.Clone());
            }

            if (contrast != 0) workerBitmap.Contrast(contrast);
            if (brightness != 0) workerBitmap.Brightness(brightness);
            if (color != 0) workerBitmap.Saturation(color);

            lock (_result)
            {
                _result.Dispose();
                _result = workerBitmap;
            }
        }

        public Bitmap TranformBitmap()
        {
            Bitmap workerBitmap;
            lock (_result)
            {
                workerBitmap = new Bitmap((Bitmap)_result.Clone());
            }

            return workerBitmap.Rotate(_rotateAngle);
        }
       
    }
}
