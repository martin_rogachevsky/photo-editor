﻿namespace PhotoEditor
{
    partial class PhotoEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.contrastLbl = new System.Windows.Forms.Label();
            this.contrastUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.colorUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.lightUpDown = new System.Windows.Forms.NumericUpDown();
            this.saveBtn = new System.Windows.Forms.Button();
            this.applyBtn = new System.Windows.Forms.Button();
            this.processingLbl = new System.Windows.Forms.Label();
            this.rotateBar = new System.Windows.Forms.TrackBar();
            this.rotateLbl = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.zoomBar = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contrastUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotateBar)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomBar)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(3, 4);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(266, 349);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // contrastLbl
            // 
            this.contrastLbl.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.contrastLbl.AutoSize = true;
            this.contrastLbl.Location = new System.Drawing.Point(558, 146);
            this.contrastLbl.Name = "contrastLbl";
            this.contrastLbl.Size = new System.Drawing.Size(46, 13);
            this.contrastLbl.TabIndex = 1;
            this.contrastLbl.Text = "Contrast";
            // 
            // contrastUpDown
            // 
            this.contrastUpDown.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.contrastUpDown.AutoSize = true;
            this.contrastUpDown.Location = new System.Drawing.Point(605, 144);
            this.contrastUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.contrastUpDown.Name = "contrastUpDown";
            this.contrastUpDown.Size = new System.Drawing.Size(51, 20);
            this.contrastUpDown.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(558, 173);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Color";
            // 
            // colorUpDown
            // 
            this.colorUpDown.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.colorUpDown.Location = new System.Drawing.Point(605, 171);
            this.colorUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.colorUpDown.Name = "colorUpDown";
            this.colorUpDown.Size = new System.Drawing.Size(51, 20);
            this.colorUpDown.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(558, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Light";
            // 
            // lightUpDown
            // 
            this.lightUpDown.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lightUpDown.Location = new System.Drawing.Point(605, 197);
            this.lightUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.lightUpDown.Name = "lightUpDown";
            this.lightUpDown.Size = new System.Drawing.Size(51, 20);
            this.lightUpDown.TabIndex = 8;
            // 
            // saveBtn
            // 
            this.saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveBtn.Location = new System.Drawing.Point(571, 387);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 10;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // applyBtn
            // 
            this.applyBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.applyBtn.Location = new System.Drawing.Point(571, 352);
            this.applyBtn.Name = "applyBtn";
            this.applyBtn.Size = new System.Drawing.Size(75, 25);
            this.applyBtn.TabIndex = 15;
            this.applyBtn.Text = "Apply";
            this.applyBtn.UseVisualStyleBackColor = true;
            this.applyBtn.Click += new System.EventHandler(this.applyBtn_Click);
            // 
            // processingLbl
            // 
            this.processingLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.processingLbl.AutoSize = true;
            this.processingLbl.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.processingLbl.Location = new System.Drawing.Point(577, 336);
            this.processingLbl.Name = "processingLbl";
            this.processingLbl.Size = new System.Drawing.Size(66, 13);
            this.processingLbl.TabIndex = 16;
            this.processingLbl.Text = "Computing...";
            this.processingLbl.Visible = false;
            // 
            // rotateBar
            // 
            this.rotateBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rotateBar.Location = new System.Drawing.Point(12, 378);
            this.rotateBar.Maximum = 360;
            this.rotateBar.Name = "rotateBar";
            this.rotateBar.Size = new System.Drawing.Size(518, 45);
            this.rotateBar.TabIndex = 17;
            this.rotateBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.rotateBar.Scroll += new System.EventHandler(this.rotateBar_Scroll);
            this.rotateBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rotateBar_MouseUp);
            // 
            // rotateLbl
            // 
            this.rotateLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.rotateLbl.AutoSize = true;
            this.rotateLbl.Location = new System.Drawing.Point(247, 397);
            this.rotateLbl.Name = "rotateLbl";
            this.rotateLbl.Size = new System.Drawing.Size(51, 13);
            this.rotateLbl.TabIndex = 18;
            this.rotateLbl.Text = "Rotate: 0";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.AutoScrollMargin = new System.Drawing.Size(10, 10);
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Location = new System.Drawing.Point(12, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(523, 352);
            this.panel1.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(589, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Zoom";
            // 
            // zoomBar
            // 
            this.zoomBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zoomBar.Location = new System.Drawing.Point(541, 8);
            this.zoomBar.Maximum = 100;
            this.zoomBar.Name = "zoomBar";
            this.zoomBar.Size = new System.Drawing.Size(131, 45);
            this.zoomBar.TabIndex = 20;
            this.zoomBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.zoomBar.Scroll += new System.EventHandler(this.zoomBar_Scroll);
            // 
            // PhotoEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 426);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.zoomBar);
            this.Controls.Add(this.rotateLbl);
            this.Controls.Add(this.rotateBar);
            this.Controls.Add(this.processingLbl);
            this.Controls.Add(this.applyBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.lightUpDown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.colorUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.contrastUpDown);
            this.Controls.Add(this.contrastLbl);
            this.Name = "PhotoEditorForm";
            this.Text = "Photo Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PhotoEditorForm_FormClosing);
            this.Load += new System.EventHandler(this.PhotoEditorForm_Load);
            this.SizeChanged += new System.EventHandler(this.PhotoEditorForm_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contrastUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotateBar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label contrastLbl;
        private System.Windows.Forms.NumericUpDown contrastUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown colorUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown lightUpDown;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button applyBtn;
        private System.Windows.Forms.Label processingLbl;
        private System.Windows.Forms.TrackBar rotateBar;
        private System.Windows.Forms.Label rotateLbl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar zoomBar;
    }
}