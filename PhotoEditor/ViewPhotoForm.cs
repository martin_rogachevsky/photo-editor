﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;

namespace PhotoEditor
{
    public partial class ViewPhotoForm : Form
    {
        public ViewPhotoForm()
        {
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (var b = Image.FromFile(openFileDialog.FileName))
                {
                    try
                    {
                        pictureBox.Image = (Bitmap)b.Clone();
                        editToolStripMenuItem1.Enabled = true;
                        saveAsToolStripMenuItem.Enabled = true;
                        closePhotoToolStripMenuItem.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, @"Error!", MessageBoxButtons.OK);
                    }
                }
            }
        }

        private void closePhotoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox.Image = null;
            editToolStripMenuItem1.Enabled = false;
            saveAsToolStripMenuItem.Enabled = false;
            closePhotoToolStripMenuItem.Enabled = false;
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    pictureBox.Image.Save(saveFileDialog.FileName, ImageFormat.Jpeg);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, @"Error!", MessageBoxButtons.OK);
                }
            }
        }

        private void photoEditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            var editForm = new PhotoEditorForm(new Bitmap(pictureBox.Image));
            if (editForm.ShowDialog() == DialogResult.OK)
            {
                pictureBox.Image = (Bitmap)editForm.Result.Clone();
            }
            editForm.Dispose();
            Show();
        }

        private void paintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            var printForm = new PhotoPainterForm(new Bitmap(pictureBox.Image));
            if (printForm.ShowDialog() == DialogResult.OK)
            {
                pictureBox.Image = (Bitmap)printForm.Result.Clone();
            }
            printForm.Dispose();
            Show();
        }
    }
}
