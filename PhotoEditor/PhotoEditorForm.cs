﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using BitmapOperations;

namespace PhotoEditor
{
    public partial class PhotoEditorForm : Form
    {
        private readonly Bitmap _source;
        private readonly EditableBitmap _workerBitmap;
        private Boolean _saveClosing;

        public Bitmap Result;

        public PhotoEditorForm(Bitmap bitmap)
        {
            InitializeComponent();

            _source = bitmap;
            pictureBox.Image = bitmap;
            _workerBitmap = new EditableBitmap(_source);
            _saveClosing = false;
        }

        private void resizePictureBox()
        {
            if (zoomBar.Value == 0)
            {
                pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
            }
            else
            {
                pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                double value = 1 - (zoomBar.Value / 100.0);

                pictureBox.Width = (int)((pictureBox.Image.Width - 0.95 * panel1.Width) * value + 0.95 * panel1.Width);
                pictureBox.Height = (int)((pictureBox.Image.Height - 0.9 * panel1.Height) * value + 0.95 * panel1.Height);

                pictureBox.Left = 0;
                pictureBox.Top = 0;
            }
        }

        private async void applyBtn_Click(object sender, EventArgs e)
        {
            var editTask = Task.Run(() =>
                _workerBitmap.EditBitmap((int) contrastUpDown.Value, (int) lightUpDown.Value, (int) colorUpDown.Value));

            processingLbl.Visible = true;

            await editTask;

            Task getImageTask;
            Bitmap b = null;
            lock (pictureBox.Image)
            {
                getImageTask = Task.Run(() => b = _workerBitmap.GetBitmap());
            }
            await getImageTask;
            pictureBox.Image = b;
            processingLbl.Visible = false;
        }

        private void rotateBar_Scroll(object sender, EventArgs e)
        {
            rotateLbl.Text = $@"Rotate: {rotateBar.Value}";

        }

        private async void rotateBar_MouseUp(object sender, MouseEventArgs e)
        {
            processingLbl.Visible = true;
            _workerBitmap.RotateAngle = rotateBar.Value;
            Task getImageTask;
            Bitmap b= null;
            lock (pictureBox.Image)
            {
                getImageTask = Task.Run(() => b = _workerBitmap.GetBitmap());
            }
            await getImageTask;
            pictureBox.Image = b;
            processingLbl.Visible = false;
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Result = new Bitmap(pictureBox.Image);
            _saveClosing = true;
            Close();
        }

        private void PhotoEditorForm_Load(object sender, EventArgs e)
        {
             
        }

        private void PhotoEditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_saveClosing || e.CloseReason != CloseReason.UserClosing)
                return;
            var dialogResult = MessageBox.Show(@"Save changes?", @"Save", MessageBoxButtons.YesNoCancel);
            if (dialogResult == DialogResult.Yes)
            {
                DialogResult = DialogResult.OK;
                Result = new Bitmap(pictureBox.Image);
            }
            else if (dialogResult == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                DialogResult = DialogResult.Abort;
            }

        }

        private void zoomBar_Scroll(object sender, EventArgs e)
        {
            resizePictureBox();
        }

        private void PhotoEditorForm_SizeChanged(object sender, EventArgs e)
        {
            resizePictureBox();
        }
    }
}
