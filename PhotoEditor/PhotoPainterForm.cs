﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoEditor
{
    public partial class PhotoPainterForm : Form
    {
        private readonly Pen _pen = new Pen(Color.Black, 5);
        private readonly Bitmap _source;
        private Point _prevPoint, _currPoint;
        private Boolean _saveClosing;

        public Bitmap Result;

        public PhotoPainterForm(Bitmap source)
        {
            InitializeComponent();

            _source = (Bitmap)source.Clone();
            pictureBox.Image = (Bitmap)_source.Clone();
        }

        private void colorBtn_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                colorPnl.BackColor = colorDialog.Color;
                _pen.Color = colorDialog.Color;
            }
        }


        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                _prevPoint = e.Location;
        }

        private void widthBar_Scroll(object sender, EventArgs e)
        {
            _pen.Width = widthBar.Value;
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                using (Graphics g = Graphics.FromImage(pictureBox.Image))
                {
                    _currPoint = e.Location;
                    g.DrawLine(_pen, _prevPoint, _currPoint);
                    _prevPoint = _currPoint;
                    pictureBox.Invalidate();
                }
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Result = new Bitmap(pictureBox.Image);
            _saveClosing = true;
            Close();
        }

        private void PhotoEditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_saveClosing || e.CloseReason != CloseReason.UserClosing)
                return;
            var dialogResult = MessageBox.Show(@"Save changes?", @"Save", MessageBoxButtons.YesNoCancel);
            if (dialogResult == DialogResult.Yes)
            {
                DialogResult = DialogResult.OK;
                Result = new Bitmap(pictureBox.Image);
            }
            else if (dialogResult == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                DialogResult = DialogResult.Abort;
            }

        }
    }
}
